# FIX

## libvlc_ArtRequest EXC_BAD_ACCESS
https://code.videolan.org/videolan/VLCKit/-/issues/116
But there shouldn't be 2 of those in play, right? Well I though so, but then I figured out that I was initing VLCMediaListPlayer with _listPlayer = [[VLCMediaListPlayer alloc] initWithOptions:options andDrawable:_actualVideoOutputView]; and with initWithOptions: somehow causes this to init new VLCLibrary every time thus causing the issue.
Swapping that init with _listPlayer = [[VLCMediaListPlayer alloc] initWithDrawable:_actualVideoOutputView]; magically made the problem disappear, because now there's only one VLCLibrary in play.

-(void)setResume:(BOOL)autoplay
注释掉 96-100 110-114 不用重新初始化 player
```
98: [_player stop];
109: _player = [[VLCMediaPlayer alloc]
```
